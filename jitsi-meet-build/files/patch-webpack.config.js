--- webpack.config.js.orig	2020-03-30 22:01:27 UTC
+++ webpack.config.js
@@ -179,13 +179,13 @@ module.exports = [
         entry: {
             'app.bundle': './app.js'
         },
-        performance: getPerformanceHints(3 * 1024 * 1024)
+        performance: getPerformanceHints(4 * 1024 * 1024)
     }),
     Object.assign({}, config, {
         entry: {
             'device_selection_popup_bundle': './react/features/settings/popup.js'
         },
-        performance: getPerformanceHints(700 * 1024)
+        performance: getPerformanceHints(1024 * 1024)
     }),
     Object.assign({}, config, {
         entry: {
@@ -254,7 +254,7 @@ module.exports = [
             filename: '[name].min.js',
             sourceMapFilename: '[name].min.map'
         }),
-        performance: getPerformanceHints(30 * 1024)
+        performance: getPerformanceHints(40 * 1024)
     }),
 
     Object.assign({}, config, {
@@ -265,7 +265,7 @@ module.exports = [
             library: 'JitsiMeetExternalAPI',
             libraryTarget: 'umd'
         }),
-        performance: getPerformanceHints(30 * 1024)
+        performance: getPerformanceHints(40 * 1024)
     })
 ];
 
